import { useState } from 'react';
import './App.css';

function App() {
  const [number, setNumber] = useState(0);

  function count() {
    setNumber(number + 1);
  }

  function reset() {
    setNumber(0);
  }

  function decrement() {
    setNumber(number - 1);
  }

  return (
    <div className="container">
      <h3>Counter Application</h3>
      <h1 className="counter">{number}</h1>
      <button onClick={count}>Increment</button>
      <button onClick={reset}>Reset</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
}

export default App;
